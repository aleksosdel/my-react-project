import React, {useState} from 'react';
import NavBar from "../components/NavBar";
import {Button, Container, Form} from "react-bootstrap";
import {useDispatch} from "react-redux";

const Auth = () => {
    const [title, setTitle] = useState('')
    const [desc, setDesc] = useState('')
    const [price, setPrice] = useState(0)
    const [file, setFile] = useState(null)

    const dispatch = useDispatch()

    const addProduct = (e) => {
        e.preventDefault()
        const data = {
            id: Date.now(),
            title: title,
            desc: desc,
            price: `${price}`,
            img: file,
        }
        dispatch({type: "CREATE_PRODUCT", data})
    }

    const selectFile = (e) => {
        setFile(e.target.files[0])
    }
    return (
        <>
            <NavBar/>
            <Container>
                <Form className='mt-5'>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" value={title} onChange={e => setTitle(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" value={desc} onChange={e => setDesc(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="text" value={price} onChange={e => setPrice(Number(e.target.value))}/>
                    </Form.Group>
                    <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Default file input example</Form.Label>
                        <Form.Control type="file" onChange={selectFile}/>
                    </Form.Group>
                    <Button variant="primary" type="submit" onClick={addProduct}>
                        Add
                    </Button>
                </Form>
            </Container>
        </>
    );
};

export default Auth;