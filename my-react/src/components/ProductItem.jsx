import React from 'react';
import {Button, Card, Col} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";

const ProductItem = ({props}) => {
    const navigate = useNavigate()

    const dispatch = useDispatch()
    const removeProduct = (id) => {
        dispatch({type: "REMOVE_PRODUCT", id})
    }
    return (
        <Col md='4' className="mt-4">
            <Card>
                <Card.Img
                    height={210}
                    variant="top"
                    src={props.img}
                    onClick={() => navigate('/' + props.id)}
                />
                <Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <Card.Text>
                        {props.desc}
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="danger">Buy</Button>
                    <Button
                        style={{width: '100%'}}
                        variant="danger"
                        onClick={() => {removeProduct(props.id)}}
                    >Remove</Button>
                </Card.Body>
            </Card>
        </Col>
    );
};

export default ProductItem;